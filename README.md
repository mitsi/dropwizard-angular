## Front
npm start -> will start a debug server on 4200 that use http:localhost:8080/users REST API

## Back
compile.sh -> compile a fat-jar
start.sh -> start the fat- jar with configuration located : ./src/main/resources/app-configuration.yml

## Configuration
inMemory: true/false  -> false: try to connect to dynamoDb instance, true: use arraylist in ram

## Features
- API
    - http://localhost:8080
- admin
    - http://localhost:8080/admin
- Sagger
    - http://localhost:8080/swagger

## Tests
/!\ Tests use local docker DynamoDb instance to test repo implementation 

> $ docker-compose up