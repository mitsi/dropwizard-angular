package dropwizard.dynamodb.repository

import dropwizard.dynamodb.AppConfiguration
import dropwizard.dynamodb.model.User
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

/**
 * Not well written tests, too much dependent
 */
internal class DynamoRepositoryTest {
    val dynamoRepository = DynamoRepository(
        AppConfiguration(
            "http://localhost:8000",
            "ak",
            "sk",
            "eu-west-1",
            SwaggerBundleConfiguration(),
            false
        )
    )

    @BeforeEach
    internal fun setUp() {
        dynamoRepository.delete()
        dynamoRepository.createTableIfNotExists()
    }

    @Test
    internal fun should_add_user() {
        dynamoRepository.add(User("a_user"))
        assertThat(dynamoRepository.get("a_user")).usingRecursiveComparison().isEqualTo(User("a_user", 0))
    }

    @Test
    internal fun should_getUsers() {
        dynamoRepository.add(User("a_user"))
        dynamoRepository.add(User("a_user2"))
        assertThat(dynamoRepository.getAll())
            .usingRecursiveComparison()
            .ignoringCollectionOrder()
            .isEqualTo(listOf(User("a_user", 0), User("a_user2", 0)))
    }

    @Test
    internal fun should_return_null_if_not_found() {
        dynamoRepository.add(User("a_user"))
        dynamoRepository.add(User("a_user2"))
        assertThat(dynamoRepository.get("another")).isNull();
    }

    @Test
    internal fun should_updateUser() {
        dynamoRepository.add(User("a_user"))
        dynamoRepository.add(User("a_user2"))

        dynamoRepository.update(User("a_user2", 18))

        assertThat(dynamoRepository.getAll())
            .usingRecursiveComparison()
            .ignoringCollectionOrder()
            .isEqualTo(listOf(User("a_user", 0), User("a_user2", 18)))
    }

    @Test
    internal fun updateUser_should_return_false_if_not_found() {
        dynamoRepository.add(User("a_user"))
        dynamoRepository.add(User("a_user2"))

        val update = dynamoRepository.update(User("a_user1", 18))

        assertThat(update).isFalse();
    }

    @Test
    internal fun should_removeUsers() {
        dynamoRepository.add(User("a_user"))
        dynamoRepository.add(User("a_user2"))

        dynamoRepository.delete()

        assertThat(dynamoRepository.getAll())
            .usingRecursiveComparison()
            .ignoringCollectionOrder()
            .isEqualTo(emptyList<User>())
    }
}