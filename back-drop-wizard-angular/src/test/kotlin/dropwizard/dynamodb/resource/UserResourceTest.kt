package dropwizard.dynamodb.resource

import dropwizard.dynamodb.model.User
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.jupiter.MockitoExtension
import org.mockito.kotlin.any
import dropwizard.dynamodb.repository.Repository
import javax.ws.rs.WebApplicationException

@ExtendWith(MockitoExtension::class)
class UserResourceTest {
    @Test
    fun getUser_throw404IfPseudoIsNull(@Mock repository: Repository) {
        val userResource = UserResource(repository)

        val errorUserNotFound = assertThrows(WebApplicationException::class.java) {
            userResource.getUser(null)
        }

        assertThat(errorUserNotFound.message).isEqualTo("User not found")
        assertThat(errorUserNotFound.response.status).isEqualTo(404)
    }

    @Test
    fun getUser_throw404IfPseudoNotFound(@Mock repository: Repository) {
        Mockito.`when`(repository.get(anyString())).thenReturn(null)
        val userResource = UserResource(repository)

        val errorUserNotFound = assertThrows(WebApplicationException::class.java) {
            userResource.getUser("Nicolas")
        }

        assertThat(errorUserNotFound.message).isEqualTo("User not found")
        assertThat(errorUserNotFound.response.status).isEqualTo(404)
    }

    @Test
    fun getUser_OK(@Mock repository: Repository) {
        val usersInDb = listOf(User("Nicolas", 8), User("Nicolas2", 18), User("Nicolas3", 4))
        val expectedUser = User("Nicolas", 8, 2)
        Mockito.`when`(repository.getAll()).thenReturn(usersInDb)
        Mockito.`when`(repository.get("Nicolas")).thenReturn(User("Nicolas", 8))
        val userResource = UserResource(repository)

        val result = userResource.getUser("Nicolas")

        assertThat(result.status).isEqualTo(200)
        assertThat(result.entity).usingRecursiveComparison().isEqualTo(expectedUser)
        verify(repository, times(1)).get(any())
        verify(repository, times(1)).getAll()
    }

    @Test
    fun addUser_OK(@Mock repository: Repository) {
        val expectedUser = User("Nicolas", 0)
        Mockito.`when`(repository.add(expectedUser)).thenReturn(true)
        val userResource = UserResource(repository)

        val result = userResource.addUser(User("Nicolas", 1))

        assertThat(result.status).isEqualTo(200)
        assertThat(result.entity).isEqualTo(expectedUser)
    }

    @Test
    fun addUser_OnExistingUserReturns403(@Mock repository: Repository) {
        val existingUser = User("Nicolas", 0)
        Mockito.`when`(repository.get("Nicolas")).thenReturn(existingUser)
        val userResource = UserResource(repository)

        val pseudoAlreadyExistsException = assertThrows(WebApplicationException::class.java) {
            userResource.addUser(User("Nicolas", 0))
        }

        assertThat(pseudoAlreadyExistsException.message).isEqualTo("User already exists")
        assertThat(pseudoAlreadyExistsException.response.status).isEqualTo(403)
        verify(repository, times(0)).add(any())
    }

    @Test
    fun addUser_NullPseudoReturns400(@Mock repository: Repository) {
        val userResource = UserResource(repository)

        val pseudoAlreadyExistsException = assertThrows(WebApplicationException::class.java) {
            userResource.addUser(User(null, null))
        }

        assertThat(pseudoAlreadyExistsException.message).isEqualTo("Pseudo can't be null or empty")
        assertThat(pseudoAlreadyExistsException.response.status).isEqualTo(400)
        verify(repository, times(0)).add(any())
        verify(repository, times(0)).get(any())
    }

    @Test
    fun addUser_EmptyPseudoReturns400(@Mock repository: Repository) {
        val userResource = UserResource(repository)

        val pseudoAlreadyExistsException = assertThrows(WebApplicationException::class.java) {
            userResource.addUser(User(""))
        }

        assertThat(pseudoAlreadyExistsException.message).isEqualTo("Pseudo can't be null or empty")
        assertThat(pseudoAlreadyExistsException.response.status).isEqualTo(400)
        verify(repository, times(0)).add(any())
        verify(repository, times(0)).get(any())
    }

    @Test
    fun getUsers_ReturnEmptyListIfEmpty(@Mock repository: Repository) {
        val userResource = UserResource(repository)
        Mockito.`when`(repository.getAll()).thenReturn(emptyList())

        val users = userResource.getUsers()

        assertThat(users.status).isEqualTo(200);
        assertThat(users.entity).isEqualTo(ArrayList<User>())
    }

    @Test
    fun getUsers_ReturnAllUsers(@Mock repository: Repository) {
        val userResource = UserResource(repository)
        val usersInDb = listOf(User("a", 1), User("b", 2))
        val expectedUsers = listOf(User("b", 2, 1), User("a", 1, 2))
        Mockito.`when`(repository.getAll()).thenReturn(usersInDb)

        val users = userResource.getUsers()

        assertThat(users.status).isEqualTo(200);
        assertThat(users.entity).isEqualTo(expectedUsers);
    }

    @Test
    fun getUsers_equallyRankedUsersHaveSameRanks(@Mock repository: Repository) {
        val userResource = UserResource(repository)
        val usersInDb = listOf(User("a", 2), User("b", 2))
        val expectedUsers = listOf(User("a", 2, 1), User("b", 2, 1))
        Mockito.`when`(repository.getAll()).thenReturn(usersInDb)

        val users = userResource.getUsers()

        assertThat(users.status).isEqualTo(200);
        assertThat(users.entity).isEqualTo(expectedUsers);
    }

    @Test
    fun editUser_OK(@Mock repository: Repository) {
        val userResource = UserResource(repository)
        val expected = User("Nicolas", 38)
        Mockito.`when`(repository.update(expected)).thenReturn(true)

        val users = userResource.editUser("Nicolas", User(38))

        assertThat(users.status).isEqualTo(200);
        assertThat(users.entity).isEqualTo(expected)
    }

    @Test
    fun editUser_NullPseudoReturns400(@Mock repository: Repository) {
        val userResource = UserResource(repository)

        val pseudoNullExecption = assertThrows(WebApplicationException::class.java) {
            userResource.editUser(null, User(38))
        }

        assertThat(pseudoNullExecption.response.status).isEqualTo(400);
        assertThat(pseudoNullExecption.message).isEqualTo("Pseudo can't be null or empty")
    }

    @Test
    fun editUser_EmptyPseudoReturns400(@Mock repository: Repository) {
        val userResource = UserResource(repository)

        val pseudoNullExecption = assertThrows(WebApplicationException::class.java) {
            userResource.editUser("", User(38))
        }

        assertThat(pseudoNullExecption.response.status).isEqualTo(400);
        assertThat(pseudoNullExecption.message).isEqualTo("Pseudo can't be null or empty")
    }

    @Test
    fun editUser_NoPointsReturns400(@Mock repository: Repository) {
        val userResource = UserResource(repository)

        val pseudoNullExecption = assertThrows(WebApplicationException::class.java) {
            userResource.editUser("Nicolas", User(points = null))
        }

        assertThat(pseudoNullExecption.response.status).isEqualTo(400);
        assertThat(pseudoNullExecption.message).isEqualTo("Points can't be null")
    }

    @Test
    fun deleteUser_OK(@Mock repository: Repository) {
        val userResource = UserResource(repository)

        val users = userResource.deleteUser()

        assertThat(users.status).isEqualTo(204)
        verify(repository, times(1)).delete()
    }
}