package dropwizard.dynamodb.repository

import dropwizard.dynamodb.model.User

interface Repository {
    fun getAll(): List<User>
    fun get(pseudo: String): User?
    fun update(userToUpdate: User): Boolean
    fun add(userToAdd: User): Boolean
    fun delete()
}