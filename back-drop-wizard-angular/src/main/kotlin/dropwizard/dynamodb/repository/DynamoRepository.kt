package dropwizard.dynamodb.repository

import com.amazonaws.AmazonServiceException
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import com.amazonaws.services.dynamodbv2.document.DynamoDB
import com.amazonaws.services.dynamodbv2.document.ItemCollection
import com.amazonaws.services.dynamodbv2.document.QueryOutcome
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap
import com.amazonaws.services.dynamodbv2.model.*
import dropwizard.dynamodb.AppConfiguration
import dropwizard.dynamodb.model.User
import java.util.logging.Logger


class DynamoRepository(appConfiguration: AppConfiguration) : Repository {
    var logger: Logger = Logger.getLogger(this.javaClass.name)

    private val dynamoDB: AmazonDynamoDB = AmazonDynamoDBClientBuilder.standard()
        .withEndpointConfiguration(
            AwsClientBuilder.EndpointConfiguration(
                appConfiguration.dbUrl,
                appConfiguration.location
            )
        )
        .withCredentials(
            AWSStaticCredentialsProvider(
                BasicAWSCredentials(
                    appConfiguration.accessKey,
                    appConfiguration.secretKey
                )
            )
        ).build()

    private val tableName = "users"
    private val keyName = "pseudo"
    private val pointsColumn = "points"

    override fun getAll(): List<User> {
        val scan = dynamoDB.scan(ScanRequest(tableName))
        return scan.items.map<MutableMap<String, AttributeValue>?, User> {
            User(it?.get(keyName)?.s, it?.get(pointsColumn)?.n?.toInt())
        }.toList()
    }

    override fun get(pseudo: String): User? {
        val client = DynamoDB(dynamoDB).getTable(tableName)
        val spec: QuerySpec = QuerySpec()
            .withKeyConditionExpression("$keyName = :_pseudo")
            .withValueMap(ValueMap().withString(":_pseudo", pseudo))

        val items: ItemCollection<QueryOutcome> = client.query(spec)
        if (items.count() == 0) {
            return null
        }

        val foundUser = items.first()
        return User(foundUser[keyName].toString(), foundUser[pointsColumn].toString().toInt())
    }

    override fun update(userToUpdate: User): Boolean {
        val table = DynamoDB(dynamoDB).getTable(tableName)
        val updateItemSpec = UpdateItemSpec().withPrimaryKey(keyName, userToUpdate.pseudo)
            .withConditionExpression("$keyName = :pseudo")
            .withUpdateExpression("set points = :points")
            .withValueMap(
                ValueMap()
                    .withString(":pseudo", userToUpdate.pseudo)
                    .withNumber(":points", userToUpdate.points)
            )
            .withReturnValues(ReturnValue.UPDATED_NEW)
        return try {
            table.updateItem(updateItemSpec).item != null
        } catch (e: AmazonServiceException) {
            logger.severe(
                "Could not update user ${userToUpdate.pseudo} to ${userToUpdate.points} because " +
                        e.errorMessage
            )
            false
        }
    }

    override fun add(userToAdd: User): Boolean {
        val map: MutableMap<String, AttributeValue> = HashMap()
        map[keyName] = AttributeValue(userToAdd.pseudo)
        map[pointsColumn] = AttributeValue().withN(userToAdd.points.toString())
        val request = PutItemRequest()
        request.tableName = tableName
        request.returnConsumedCapacity = ReturnConsumedCapacity.TOTAL.name
        request.returnValues = ReturnValue.ALL_OLD.name
        request.item = map

        return try {
            dynamoDB.putItem(request) != null
        } catch (e: AmazonServiceException) {
            logger.severe("Could not add user ${userToAdd.pseudo} because " + e.errorMessage)
            false
        }
    }

    override fun delete() {
        dynamoDB.deleteTable(tableName)
        createTableIfNotExists()
    }

    fun createTableIfNotExists() {
        if (dynamoDB.listTables().tableNames.contains(tableName)) return

        val request = CreateTableRequest()
        request.tableName = tableName
        val attributeDefinitions: List<AttributeDefinition> =
            listOf(AttributeDefinition(keyName, ScalarAttributeType.S))
        request.setAttributeDefinitions(attributeDefinitions)

        val keySchema: List<KeySchemaElement> = listOf(KeySchemaElement(keyName, KeyType.HASH))
        request.provisionedThroughput = ProvisionedThroughput(1, 1)
        request.setKeySchema(keySchema)
        try {
            dynamoDB.createTable(request)
        } catch (e: AmazonServiceException) {
            logger.severe("Could not create database because " + e.errorMessage)
        }
    }
}
