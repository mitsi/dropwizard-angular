package dropwizard.dynamodb.repository

import dropwizard.dynamodb.model.User

class MemoryRepository: Repository {

    private val users: ArrayList<User> = ArrayList();

    override fun getAll(): List<User> {
        return users;
    }

    override fun get(pseudo: String): User? {
        return users.find { user -> user.pseudo.equals(pseudo) }
    }

    override fun update(userToUpdate: User): Boolean {
        val user = users.find { user -> user.pseudo.equals(userToUpdate.pseudo) } ?: return false
        user.points = userToUpdate.points
        return true
    }

    override fun add(userToAdd: User): Boolean {
        val user = users.find { user -> user.pseudo.equals(userToAdd.pseudo) }
        if (user != null) {
            return false
        }
        users.add(userToAdd)
        return true
    }

    override fun delete() {
        users.clear()
    }
}