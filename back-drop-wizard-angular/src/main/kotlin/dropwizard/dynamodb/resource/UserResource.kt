package dropwizard.dynamodb.resource

import dropwizard.dynamodb.error.UserAlreadyExistsExeption
import dropwizard.dynamodb.error.UserNotFoundException
import dropwizard.dynamodb.model.User
import dropwizard.dynamodb.repository.Repository
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/users")
@Api("UserResource", description = "User REST controller")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
class UserResource(private val repository: Repository) {
    @GET
    @ApiOperation(value = "Get user", response = User::class)
    @ApiResponses(ApiResponse(code = 200, message = "Return all users sorted by points"))
    fun getUsers(): Response {
        return Response.ok(getUsersDescending()).build()
    }

    @POST
    @ApiOperation(value = "Add user ", response = User::class)
    @ApiResponses(
        ApiResponse(code = 200, message = "Add the given user with 0 point"),
        ApiResponse(code = 400, message = "Pseudo is null or empty"),
        ApiResponse(code = 403, message = "User already exists")
    )
    fun addUser(user: User): Response {
        if (user.pseudo.isNullOrEmpty()) {
            throw BadRequestException("Pseudo can't be null or empty")
        }

        val foundedUser = repository.get(user.pseudo!!)
        if (foundedUser != null) {
            throw UserAlreadyExistsExeption()
        }
        user.points = 0
        repository.add(user)
        return Response.ok(user).build()
    }

    @GET
    @Path("/{pseudo}")
    @ApiOperation(value = "Get a given user by its pseudo")
    @ApiResponses(
        ApiResponse(code = 200, message = "Get user with points and ranking"),
        ApiResponse(code = 404, message = "User not found")
    )
    fun getUser(@PathParam("pseudo") pseudo: String?): Response {
        if (pseudo.isNullOrEmpty()) throw UserNotFoundException()

        val user = repository.get(pseudo) ?: throw UserNotFoundException()
        val users = this.getUsersDescending()
        val playerRank = users.indexOfFirst { u -> u.pseudo == pseudo } + 1
        user.rank = playerRank
        return Response.ok(user).build()
    }

    @PUT
    @Path("/{pseudo}")
    @ApiOperation(value = "Delete Users", response = User::class)
    @ApiResponses(
        ApiResponse(code = 200, message = "Update user's points"),
        ApiResponse(code = 400, message = "Pseudo or points are null or empty"),
        ApiResponse(code = 404, message = "User not foudn")
    )
    fun editUser(@PathParam("pseudo") pseudo: String?, user: User): Response {
        if (pseudo.isNullOrEmpty()) {
            throw BadRequestException("Pseudo can't be null or empty")
        }
        if (user.points == null) {
            throw BadRequestException("Points can't be null")
        }

        user.pseudo = pseudo
        if (!repository.update(user)) {
            throw UserNotFoundException()
        }
        return Response.ok(user).build();
    }

    @DELETE
    @ApiOperation(value = "Delete Users")
    @ApiResponses(ApiResponse(code = 200, message = "Delete all users"))
    fun deleteUser(): Response {
        repository.delete()
        return Response.ok().status(Response.Status.NO_CONTENT).build()
    }

    private fun getUsersDescending(): List<User> {
        val sortedByDescending = repository.getAll().sortedByDescending { user -> user.points }
        if (sortedByDescending.isEmpty()) return emptyList()

        var index = 1
        var rank = 1
        sortedByDescending[0].rank = rank
        while (index < sortedByDescending.size) {
            if (sortedByDescending[index].points!! < sortedByDescending[index - 1].points!!) {
                rank++;
            }
            sortedByDescending[index].rank = rank
            index++
        }
        return sortedByDescending
    }
}
