package dropwizard.dynamodb.model

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonPropertyOrder
@JsonInclude(JsonInclude.Include.NON_NULL)
data class User constructor(var pseudo: String?, var points: Int?, var rank: Int?){
    constructor(pseudo: String?): this(pseudo, 0, null)
    constructor(points: Int?): this(null, points, null)
    constructor(pseudo: String?, points: Int?): this(pseudo, points, null)
}