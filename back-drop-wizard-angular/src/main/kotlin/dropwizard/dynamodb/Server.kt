package dropwizard.dynamodb

import com.codahale.metrics.servlets.AdminServlet
import com.codahale.metrics.servlets.HealthCheckServlet
import com.codahale.metrics.servlets.MetricsServlet
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.module.kotlin.KotlinModule
import dropwizard.dynamodb.repository.DynamoRepository
import dropwizard.dynamodb.repository.MemoryRepository
import dropwizard.dynamodb.repository.Repository
import dropwizard.dynamodb.resource.UserResource
import io.dropwizard.Application
import io.dropwizard.Configuration
import io.dropwizard.jetty.NonblockingServletHolder
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment
import io.federecio.dropwizard.swagger.SwaggerBundle
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration
import org.eclipse.jetty.servlets.CrossOriginFilter
import java.util.*
import java.util.logging.Logger
import javax.servlet.DispatcherType


class Server : Application<AppConfiguration>() {
    var logger: Logger = Logger.getLogger(this.javaClass.name)

    companion object {
        @JvmStatic
        fun main(args: Array<String>) = Server().run(*args)
    }

    override fun run(appConfiguration: AppConfiguration, environment: Environment) {
        val repository: Repository
        if (appConfiguration.inMemory == true) {
            logger.info("Starting in memory db")
            repository = MemoryRepository()
        } else {
            repository = DynamoRepository(appConfiguration)
            repository.createTableIfNotExists()
        }

        environment.jersey().register(UserResource(repository))

        environment.applicationContext.setAttribute(MetricsServlet.METRICS_REGISTRY, environment.metrics())
        environment.applicationContext.setAttribute(
            HealthCheckServlet.HEALTH_CHECK_REGISTRY,
            environment.healthChecks()
        )
        environment.applicationContext.addServlet(NonblockingServletHolder(AdminServlet()), "/admin/*")

        val cors = environment.servlets().addFilter("CORS", CrossOriginFilter::class.java)
        cors.setInitParameter("allowedOrigins", "*")
        cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin")
        cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD")
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType::class.java), true, "/*")
    }

    override fun initialize(bootstrap: Bootstrap<AppConfiguration>) {
        bootstrap.objectMapper.registerModule(KotlinModule())
        bootstrap.addBundle(object : SwaggerBundle<AppConfiguration>() {
            override fun getSwaggerBundleConfiguration(configuration: AppConfiguration): SwaggerBundleConfiguration {
                return configuration.swaggerBundleConfiguration
            }
        })
    }
}

class AppConfiguration(
    @JsonProperty("dbUrl") val dbUrl: String,
    @JsonProperty("accessKey") val accessKey: String,
    @JsonProperty("secretKey") val secretKey: String,
    @JsonProperty("location") val location: String,
    @JsonProperty("swagger") val swaggerBundleConfiguration: SwaggerBundleConfiguration,
    @JsonProperty("inMemory") val inMemory: Boolean?
) : Configuration()