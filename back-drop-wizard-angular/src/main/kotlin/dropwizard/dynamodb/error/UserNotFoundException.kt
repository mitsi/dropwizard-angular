package dropwizard.dynamodb.error

import javax.ws.rs.WebApplicationException
import javax.ws.rs.core.Response

class UserNotFoundException : WebApplicationException("User not found", Response.Status.NOT_FOUND)