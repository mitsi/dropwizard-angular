package dropwizard.dynamodb.error

import javax.ws.rs.WebApplicationException
import javax.ws.rs.core.Response

class UserAlreadyExistsExeption: WebApplicationException("User already exists", Response.Status.FORBIDDEN)