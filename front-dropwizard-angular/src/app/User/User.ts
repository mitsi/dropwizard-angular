export class User {

    constructor(
      public pseudo: string,
      public points?: number,
      public rank?: number,
    ) {  }
  
  }