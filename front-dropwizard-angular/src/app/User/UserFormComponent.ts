import { Component, Input } from "@angular/core";
import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};

import { User } from './User';

@Component({
  selector: 'user-form',
  templateUrl: './user-form.component.html',
})
export class UserFormComponent {
  @Input() showUpdate: boolean = false
  @Input() buttonLabel: string = ""
  @Input() callback: any

  model = new User("");
  submitted = false;

  onClick() {
    if (this.model.pseudo === "") return;
    if (this.showUpdate && (this.model.points === undefined)) return
    this.callback(this.model)
    this.model.pseudo = ""
    this.model.points = undefined
  }

}