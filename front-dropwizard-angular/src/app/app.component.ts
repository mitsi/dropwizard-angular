import { Component, OnInit } from '@angular/core';
import { CrudService } from './config/config.service';
import { User } from './User/User';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'User contest';

  constructor(public crudService: CrudService) { }
  ngOnInit(): void {
    this.getUsers();
  }

  users: User[] = [];
  userInfo: User | null | undefined;
  error: string | null | undefined;

  createUser = (user: User) => {
    this.clearNotifications()
    return this.crudService.create(user).subscribe((data: {}) => {
      this.getUsers();
    }, (err: Error) => {
      this.error = err.message
    })
  }

  getUser = (user: User) => {
    this.clearNotifications()
    return this.crudService.getUser(user.pseudo).subscribe((data: User) => {
      this.userInfo = data;
    }, (err: Error) => {
      this.error = err.message
    })
  }

  getUsers = () => {
    this.clearNotifications()
    return this.crudService.getUsers().subscribe((data = []) => {
      this.users = data;
    }, (err: Error) => {
      this.error = err.message
    })
  }

  updateUser = (user: User) => {
    this.clearNotifications()
    return this.crudService.update(user.pseudo, user).subscribe((data: {}) => {
      this.getUsers();
    }, (err: Error) => {
      this.error = err.message
    })
  }

  resetContest = () => {
    this.clearNotifications()
    return this.crudService.delete().subscribe((data: {}) => {
      this.users = []
    }, (err: Error) => {
      this.error = err.message
    })
  }

  clearNotifications() {
    this.userInfo = undefined
    this.error = undefined
  }
}