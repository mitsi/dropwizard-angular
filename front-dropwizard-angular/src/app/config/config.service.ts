import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { User } from '../User/User';

@Injectable({
    providedIn: 'root'
})

export class CrudService {

    endPoint = 'http://localhost:8080';

    constructor(private httpClient: HttpClient) { }

    httpHeader = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    }

    getUsers(): Observable<User[]> {
        return this.httpClient.get<User[]>(this.endPoint + '/users')
            .pipe(
                retry(1),
                catchError(this.httpError)
            )
    }

    getUser(pseudo: string): Observable<User> {
        return this.httpClient.get<User>(this.endPoint + '/users/' + pseudo)
            .pipe(
                retry(1),
                catchError(this.httpError)
            )
    }

    create(user: User): Observable<User> {
        return this.httpClient.post<User>(this.endPoint + '/users', JSON.stringify(user), this.httpHeader)
            .pipe(
                retry(1),
                catchError(this.httpError)
            )
    }

    update(pseudo: string, user: User): Observable<User> {
        return this.httpClient.put<User>(this.endPoint + '/users/' + pseudo, JSON.stringify(user), this.httpHeader)
            .pipe(
                retry(1),
                catchError(this.httpError)
            )
    }

    delete() {
        return this.httpClient.delete<User>(this.endPoint + '/users/', this.httpHeader)
            .pipe(
                retry(1),
                catchError(this.httpError)
            )
    }

    httpError(error: any) {
        return throwError(error.error)
    }

}